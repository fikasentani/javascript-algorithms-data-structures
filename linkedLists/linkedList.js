'use strict'
//create a new Node = new node()
//create a new list = newlist

class Node {
    constructor(num) {
        this.number = num;
        this.next = null;
    }
}

class LinkedList {
    constructor(head = null, tail = null) {
        this.head = head;
        this.tail = tail;
    }
    //private modifier becuase an existing or use a check to run only if there is no node
    addNode(num) {
        let node = new Node(num);
        //if head is null that means tail is null so no need ot check if tail is null- NOT ?
        if (!this.head) {
            this.head = node;
            this.tail = node;
        }
    }

    appendNode(num) {
        if (!this.head) {
            this.addNode(num);
        } else {
            let node = new Node(num);
            this.tail.next = node;
            this.tail = node;
            // node.next = this.head;
            // this.head = node;
        }
    }

    prependNode(num) {
        if (!this.head) {
            this.addNode(num);
        } else {
            let node = new Node(num);
            node.next = this.head;
            this.head = node;
        }
    }

    printList() {
        let current = this.head;
        while (current) {
            console.log(current.number);
            current = current.next;
        }
    }

    length() {
        let current = this.head;
        let traversedTimes = 0
        while (current) {
            traversedTimes++;
            current = current.next;
        }
        return traversedTimes;
    }

    turnToArray() {
        let current = this.head;
        let myArray = [];
        while (current) {
            myArray.push(current.number);
            current = current.next;
        }
        return myArray;
    }

    removeFirstNode() {
        if (!this.head) {
            //when list is empty
            return;
        } if (!this.head.next) {
            //if list has 1 item
            this.head = null;
            this.tail = null;
        } else {
            //when list has more than 1 items
            this.head = this.head.next;
        }
    }

    indexOf(number) {
        //return the value of the index
        //if not founf return -1

        //if we searcing for number 3, check if this.head.numer === 3, if not current == this.head.next
        let current = this.head; //what node I am curenlty at
        let index = 0;
        let found = current && current.number === number;
        while (current && !found) {
            current = current.next;
            found = current && current.number === number;
            index++;
        }
        return found ? index : -1;
    }

    exists(number) {
        let current = this.head;

        while (current && current.number !== number) { // we stop if we find the number
            current = current.next;
        }
        return !!current; //we stop when current is null

    }

    remove(number) {
        // edge 1 case empty list - head is null
        // edge case 2 the item is not in the list - break and return false
        // case 3 we find the number
        // fist iterate through the list to see if it exists
        // second point its previous to its next 
        //var hold current
        // IF current.next.number === number
        // THEN current.next -> current.next.next 
        let current = this.head;

        if (current && current.number === number) {
            this.head = current.next;
        } else {
            while (current && current.next && current.next.number !== number) {
                current = current.next;
            }
            if (current && current.next && current.next.number === number) {
                current.next = current.next.next;
            }
        }
    }

    removeLast() {
        //identify previous --> current.next
        //if previous.next.number === nul 
        //THEN this.tail = current (previous)

        //case empty list
        let current = this.head;

        if (!current) {
            //empty list
            return;
        }

        if (!current.next) {
            // 1 item in list
            this.tail = null;
            this.head = null;
            return;
        }
        while (current.next.next) {

            current = current.next;
        }
        current.next = null;
        this.tail = current;
    }

    reverse() {
        let current = this.head;
        let previous = null;
        let next = null;

        while (current !== null) {
            next = current.next; //point next to current.next
            current.next = previous; //reverse their positions
            previous = current; // move previous a step forward
            current = next; // move next a step forward
        }
        this.head = previous; // assign head to new previous




    }
}

const linkedList = new LinkedList();
linkedList.prependNode(2);
linkedList.appendNode(3);
linkedList.appendNode(4);
linkedList.appendNode(5);
linkedList.appendNode(6);
linkedList.prependNode(1);
linkedList.prependNode(0);
// linkedList.printList();
// console.log(linkedList.turnToArray());
// console.log(linkedList.length());
// console.log('before',linkedList);
// linkedList.removeFirstNode();
// console.log(linkedList);
linkedList.printList();
// console.log(linkedList.indexOf(3));
// linkedList.remove(2);
console.log('--------------------------');
// linkedList.printList();
// linkedList.removeLast()
linkedList.reverse();
linkedList.printList();

// console.log('--',linkedList.head);
// console.log();
// console.log(linkedList.exists(0));
// console.log(linkedList.exists(1));
// console.log(linkedList.exists(2));